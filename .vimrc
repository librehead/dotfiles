" execute pathogen#infect()

set rtp+=~/.fzf/

filetype plugin indent on
syntax on

" color gruvbox
" colorscheme PaperColor

" let g:gruvbox_contrast_dark="medium"
" let g:gruvbox_contrast_light="medium"

set colorcolumn=120

set number relativenumber
set encoding=utf8

set nobackup
set nowb
set noswapfile

set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

" set list
set smartindent

let mapleader=","

" Swap useless caps lock to escape
" inoremap ,, <ESC>
nnoremap <leader>w <ESC>:w<CR>
nnoremap <leader>q <ESC>:q!<CR>

" Temporary
nnoremap ,c :!pdflatex AdaLovelace.tex<CR>
nnoremap ,b :!biber AdaLovelace<CR>
nnoremap ,r :!./run.sh<CR>

nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

inoremap {<CR> {<CR>}<ESC>kA<CR>

" NERDTree [t - tree]
" nnoremap <leader>t <ESC>:NERDTreeToggle<CR>

" YouCompleteMe [y - ycm] [d - declaration] [i - include]
" nnoremap <leader>yd <ESC>:YcmCompleter GoToDeclaration<CR>
" nnoremap <leader>yi <ESC>:YcmCompleter GoToInclude<CR>
" nnoremap <leader>yb <ESC>:YcmCompleter GoToDefinition<CR>
" nnoremap <leader>yt <ESC>:YcmCompleter GetType<CR>
" 
" let g:ycm_confirm_extra_conf = 0

" LanguageServer
"let g:LanguageClient_serverCommands = {
 "   \ 'cpp': ['clangd-7'],
  "  \ }

"nnoremap <leader>cw <ESC>:call LanguageClient#textDocument_rename()<CR>
"nnoremap <leader>d <ESC>:call LanguageClient#textDocument_definition()<CR>
"nnoremap <leader>b <ESC>:call LanguageClient#textDocument_implementation()<CR>

" Chromatica
" let g:chromatica#libclang_path='/usr/lib/llvm-7/lib'
" let g:chromatica#enable_at_startup=1
" let g:chromatica#responsive_mode=1

" nnoremap <leader>c :ChromaticaStart<CR>

" tnoremap <Esc> <C-\><C-n>

