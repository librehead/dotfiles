#!/bin/bash

shopt -s expand_aliases

alias apti="sudo apt install"
alias aptu="sudo apt update && sudo apt upgrade"
alias mkycm="~/./mkycm"
alias mkiarena="~/./mkiarena"
alias mkrun="~/./scripts/mkrun"
alias mkdel="~/./scripts/mkdel"
alias mkrunpy="~/mkrunpy"
alias mkcmake="~/./mkcmake"
alias mkcpp="~/./mkcpp"
alias cpproj="~/scripts/cpproj.py"
alias vim="nvim"
alias search="~/./search.py"
alias emacs="emacs -nw"
alias i3lock="i3lock -i /usr/share/backgrounds/ubuntu-default-greyscale-wallpaper.png"
alias ls="lsd -al"
alias ll="lsd -alR"
alias lr="lsd -aR"
alias lt="lsd -t"
alias la="\ls -A"
alias kp="~/scripts/kill_process.sh"
alias cling="~/Downloads/cling_2020-04-02_ubuntu18/bin/cling"
