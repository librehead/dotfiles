set runtimepath^=~/.vim runtimepath+=~/.vim/after
set runtimepath+=~/.vim/bundle/LanguageClient-neovim
let &packpath = &runtimepath
source ~/.vimrc

" call deoplete#enable()
" Switch to vim-plug
call plug#begin()

"Plug 'Shougo/deoplete.nvim', { 'do': 'UpdateRemotePlugins' }

let g:deoplete#enable_at_startup=1

"Plug 'autozimu/LanguageClient-neovim', {
"    \ 'branch': 'next',
"    \ 'do': 'bash install.sh',
"    \ }

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'jackguo380/vim-lsp-cxx-highlight'

Plug 'junegunn/fzf'

Plug 'morhetz/gruvbox'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'terryma/vim-multiple-cursors'

Plug 'rhysd/vim-clang-format'

Plug 'rust-lang/rust.vim'

Plug 'SirVer/ultisnips'

Plug 'google/yapf', {'rtp': 'plugins/vim', 'for': 'python' }

Plug 'peterhoeg/vim-qml'

Plug 'sakhnik/nvim-gdb', { 'do': ':!./install.sh \| UpdateRemotePlugins' }

call plug#end()

set list

tnoremap <Esc> <C-\><C-n>

set hidden
set nobackup
set nowritebackup

set cmdheight=2

set encoding=utf-8
set fileencoding=utf-8

" coc.nvim config
set updatetime=300
" manually request completion with ctrl-space
inoremap <silent><expr> <c-space> coc#refresh()
" confirm completion on <cr>(enter)
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" show all diagnostigc
nnoremap <silent> <leader>e :<C-u>CocList diagnostics<CR>
" find current symbol
nnoremap <leader>o :<C-u>CocList outline<CR>
" search symbol
nnoremap <leader>s :<C-u>CocList -I symbols<CR>
" try to fix error
nmap <leader>c <Plug>(coc-fix-current)
" refactor
nmap <leader>r <Plug>(coc-refactor)

let g:UltiSnipsExpandTrigger='<tab>'
let g:UltiSnipsJumpForwardTrigger='<c-j>'
let g:UltiSnipsJumpBackwardTrigger='<c-k>'
let g:UltiSnipsSnippetDirectories=["UltiSnips", "my_snippets"]

color gruvbox
let g:gruvbox_contrast_dark="medium"
let g:gruvbox_contrast_light="medium"

highlight Normal ctermbg=none
highlight NonText ctermbg=none

let g:rustfmt_autosave = 1

let g:clang_format#auto_format = 1

let s:ccls_settings = {
    \ 'highlight': { 'lsRanges' : v:true },
    \ }

let s:ccls_command = ['ccls', '--log-file=/tmp/cc.log', 'init=' . json_encode(s:ccls_settings)]

"let g:LanguageClient_serverCommands = {
"            \ 'c': ['ccls', '--log-file=/tmp/cc.log'],
"            \ 'cpp': ['ccls', '--log-file=/tmp/cc.log'],
"            \ 'cuda': ['ccls', '--log-file=/tmp/cc.log'],
"            \ 'objc': ['ccls', '--log-file=/tmp/cc.log'],
"            \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
"            \ 'python': ['/usr/local/bin/pyls'],
"            \ }

"let g:LanguageClient_loadSettings = 1
"let g:LanguageClient_settingsPath = '/home/alex/.config/nvim/settings.json'

hi LspCxxHlGroupEnumConstant ctermfg=142 cterm=none gui=none
hi LspCxxHlGroupNamespace ctermfg=229 cterm=none gui=none
hi LspCxxHlGroupMemberVariable ctermfg=223
hi LspCxxHlSymClass ctermfg=106
hi LspCxxHlSymStruct ctermfg=106
hi LspCxxHlSymEnum ctermfg=106
hi LspCxxHlSymTypeAlias ctermfg=72
hi link LspCxxHlSymTypeParameter Normal
hi LspCxxHlSymFunction ctermfg=250
hi link LspCxxHlSymMethod LspCxxHlSymFunction
hi link LspCxxHlSymStaticMethod LspCxxHlSymFunction
hi link LspCxxHlSymConstructor LspCxxHlSymFunction
hi LspCxxHlSymMacro ctermfg=203

" nnoremap <leader>cw <ESC>:call LanguageClient#textDocument_rename()<CR>
nmap <leader>cw <Plug>(coc-rename)
" nnoremap <leader>d <ESC>:call LanguageClient#textDocument_definition()<CR>
nmap <leader>d <Plug>(coc-definition)
" nnoremap <leader>b <ESC>:call LanguageClient#textDocument_implementation()<CR>
nmap <leader>b <Plug>(coc-implementation)
" nnoremap <leader>ref <ESC>:call LanguageClient#textDocument_references()<CR>
nmap <leader>ref <Plug>(coc-references)

" code formatting
au FileType cpp,hpp :nnoremap <leader>f <ESC>:ClangFormat<CR>
au FileType rust :nnoremap <leader>f <ESC>:!rustfmt %:p<CR>
au FileType python :nnoremap <leader>f <ESC>:call yapf#YAPF()<CR>

"nnoremap <leader>l <ESC>:!latexmk -pdf %:p<CR>
" nnoremap <leader>call <ESC>:call LanguageClient#findLocations({'method':'$ccls/call'})<CR>
nnoremap <leader>call <ESC>:call CocLocations('ccls','$ccls/call', {'hierarchy':v:true})<CR>
" nnoremap <leader>Call <ESC>:call LanguageClient#findLocations({'method':'$ccls/call', 'callee':v:true})<CR>
nnoremap <leader>Call <ESC>:call CocLocations('ccls','$ccls/call',{'hierarchy':v:true, 'callee':v:true})<CR>

" nnoremap K <ESC>:call LanguageClient#textDocument_hover()<CR>
nmap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
    if (index(['vim', 'help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
    else
        call CocAction('doHover')
    endif
endfunction

" Quick run for MIPS programs
au FileType asm :nnoremap <F9> <ESC>:terminal spim -file %:p<CR>

au FileType cpp,hpp :nnoremap <F9> <ESC>:terminal<CR>A cd $(~/scripts/try_find.py) && ./run.sh<CR>
au FileType cpp,hpp :nnoremap <F10> <ESC>:terminal <CR>A cd $(~/scripts/try_find.py) && ./delete.sh<CR>
au FileType cpp,hpp :nnoremap <F11> <ESC>:terminal <CR>A cd $(~/scripts/try_find.py) && ./delete.sh && ./run.sh<CR>
au FileType python :nnoremap <F9> <ESC>:terminal python3 %:p<CR>

au FileType rust :nnoremap <F7> <ESC>:!cargo test<CR>
au FileType rust :nnoremap <F8> <ESC>:!cargo build<CR>
au FileType rust :nnoremap <F9> <ESC>:!cargo run<CR>
au FileType rust :nnoremap <F10> <ESC>:!cargo clean<CR>
au FileType rust :nnoremap <F11> <ESC>:!cargo clean && cargo build<CR>

au FileType tex :nnoremap <leader>l <ESC>:!latexmk -pdf %:p<CR>
au FileType tex :nnoremap <F9> <ESC>:terminal latexmk -pdf %:p<CR>A
au FileType tex :nnoremap <leader>ctx <ESC>:!context %:p<CR>

au FileType cpp,hpp :vnoremap <leader>f :ClangFormat<CR>

au BufNewFile,BufRead *.context set filetype=tex

nnoremap <leader><leader>s aș
nnoremap <leader><leader>S aȘ
nnoremap <leader><leader>i aî
nnoremap <leader><leader>I aÎ
nnoremap <leader><leader>t aț
nnoremap <leader><leader>T aȚ
nnoremap <leader><leader>a aă
nnoremap <leader><leader>A aÂ
nnoremap <leader><leader>m aâ
nnoremap <leader><leader>M aĂ
