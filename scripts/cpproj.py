#!/usr/bin/env python3

from typing import List

import argparse
import os

parser = argparse.ArgumentParser()

parser.add_argument('name', help='Name of the executable/library')
parser.add_argument('-l',
                    '--lib',
                    help='Create a library, not an executable',
                    action='store_true')
args = parser.parse_args()

executable_template: str = '~/scripts/cmake_executable_template/.'
library_template: str = '~/scripts/cmake_library_template/.'

def join(path1: str, path2: str) -> str:
    return os.path.join(path1, path2)

try:
    proj_path: str = join(os.path.abspath(os.getcwd()), args.name)
    os.mkdir(proj_path)
    os.chdir(proj_path)

    src_dir: str = library_template if args.lib == True else executable_template

    os.system(f"cp -r {src_dir} .")

except FileExistsError:
    print("Directory already exists!")
