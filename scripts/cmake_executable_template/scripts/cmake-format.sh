#!/usr/bin/env bash

cmake-format -i ./CMakeLists.txt
cmake-format -i ./src/CMakeLists.txt
cmake-format -i ./cmake/SetCXXStandard.cmake
cmake-format -i ./cmake/CompileArgs.cmake
