#!/bin/bash

export CC=clang
export CXX=clang++

#export CC=gcc
#export CXX=g++

cmake -DCMAKE_BUILD_TYPE=Debug \
      -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
      ..

mv compile_commands.json ..

cmake --build .
./src/main
