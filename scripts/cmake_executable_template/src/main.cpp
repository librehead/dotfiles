#include "main.hpp"

auto main(int const, char const* const[]) noexcept -> int
{
    using namespace std::string_literals;
    print("Hello world!"s);
}
