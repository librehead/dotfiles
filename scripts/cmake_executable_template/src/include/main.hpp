#ifndef PROJ_HPP
#define PROJ_HPP
#pragma once

#include <iostream>
#include <string>

template<typename Ch>
auto print(std::basic_string<Ch> const& str) noexcept -> void
{
    std::cout << str << std::endl;
}

#endif // !PROJ_HPP
