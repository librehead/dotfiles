#ifndef PROJ_HPP
#define PROJ_HPP
#pragma once

#include <string>

auto print(std::string const& str) noexcept -> void;

#endif // !PROJ_HPP
