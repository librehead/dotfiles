#include <iostream>

#include "lib.hpp"

auto print(std::string const& str) noexcept -> void
{
    std::cout << str << std::endl;
}
