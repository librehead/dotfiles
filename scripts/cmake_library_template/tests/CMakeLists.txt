function(build_test TEST_NAME TEST_FILE)
  add_executable(${TEST_NAME} ${TEST_FILE})
  target_link_libraries(${TEST_NAME} PRIVATE ${CMAKE_PROJECT_NAME})
  set_cxx_standard(${TEST_NAME} 17)
  target_compile_options(${TEST_NAME} PRIVATE ${COMPILE_ARGS})
  add_test(${TEST_NAME} ${TEST_NAME})
endfunction()

build_test(basic ${CMAKE_CURRENT_SOURCE_DIR}/basic.cpp)
