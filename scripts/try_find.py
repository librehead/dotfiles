#!/usr/bin/python3

import os
import subprocess
import sys

depth = 1

search_obj = "build"

if len(sys.argv) >= 2:
    search_obj = sys.argv[1]

while depth <= 5:
    result = subprocess.run(["find", ".", "-maxdepth", "1", "-name", search_obj],\
            capture_output=True)

    if len(result.stdout) > 0:
        full_path = os.path.abspath(result.stdout.splitlines()[0])
        print(full_path.decode('ascii'))
        break

    os.chdir("..")
    depth += 1

